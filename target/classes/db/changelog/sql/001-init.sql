CREATE SEQUENCE role_id_seq;

CREATE TABLE role (
    id integer PRIMARY KEY DEFAULT nextval('role_id_seq'::regclass) NOT NULL,
    name text NOT NULL
);

CREATE SEQUENCE genre_id_seq;

CREATE TABLE genre (
    id integer PRIMARY KEY DEFAULT nextval('genre_id_seq'::regclass),
    name text NOT NULL
);

CREATE SEQUENCE account_id_seq;

CREATE TABLE account (
    id integer PRIMARY KEY DEFAULT nextval('account_id_seq'::regclass),
    firstname text,
    lastname text,
    email text NOT NULL,
    password text NOT NULL,
    role_id integer,
    CONSTRAINT account_role_id_fkey
        FOREIGN KEY (role_id)
        REFERENCES role(id)
);

CREATE SEQUENCE author_id_seq;

CREATE TABLE author (
    id integer PRIMARY KEY DEFAULT nextval('author_id_seq'::regclass),
    name text DEFAULT 'unknown'
);

CREATE SEQUENCE album_id_seq;

CREATE TABLE album (
    id integer PRIMARY KEY DEFAULT nextval('album_id_seq'::regclass),
    name text DEFAULT 'undefined',
    year integer,
    author_id integer,
    CONSTRAINT album_author_id_fkey
        FOREIGN KEY (author_id)
        REFERENCES author(id)
);

CREATE SEQUENCE track_id_seq;

CREATE TABLE track (
    id integer PRIMARY KEY DEFAULT nextval('track_id_seq'::regclass),
    name text DEFAULT 'undefined',
    file_name text NOT NULL,
    author_id integer,
    album_id integer,
    genre_id integer,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT track_author_id_fkey
        FOREIGN KEY (author_id)
        REFERENCES author(id),
    CONSTRAINT track_album_id_fkey
        FOREIGN KEY (album_id)
        REFERENCES album(id),
    CONSTRAINT track_genre_id_fkey
        FOREIGN KEY (genre_id)
        REFERENCES genre(id)
);

CREATE TABLE favorite_tracks (
    account_id integer,
    track_id integer,
    PRIMARY KEY (account_id, track_id),
    CONSTRAINT favorite_tracks_account_id_fkey
        FOREIGN KEY (account_id)
        REFERENCES account(id),
    CONSTRAINT favorite_tracks_track_id_fkey
        FOREIGN KEY (track_id)
        REFERENCES track(id)
);

CREATE SEQUENCE playlist_id_seq;

CREATE TABLE playlist (
    id integer PRIMARY KEY DEFAULT nextval('playlist_id_seq'::regclass),
    name text NOT NULL,
    account_id integer NOT NULL,
    CONSTRAINT playlist_account_id_fkey
        FOREIGN KEY (account_id)
        REFERENCES account(id)
);

CREATE TABLE playlist_to_track (
    playlist_id integer,
    track_id integer,
    PRIMARY KEY (playlist_id, track_id),
    CONSTRAINT account_playlist_account_id_fkey
        FOREIGN KEY (playlist_id)
        REFERENCES playlist(id),
    CONSTRAINT account_playlist_track_id_fkey
        FOREIGN KEY (track_id)
        REFERENCES track(id)
);







