package com.musicsevice.controller;

import com.musicsevice.model.Track;
import com.musicsevice.service.TrackService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.ws.rs.core.MediaType;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.List;


@RestController
@CrossOrigin(methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.DELETE})
@RequestMapping(value = "/api/track", produces = MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
public class TrackController {

    private final TrackService trackService;

    @PostMapping
    public Track uploadTrack(@RequestParam("file") MultipartFile file) throws Exception {
        return trackService.uploadTrack(file);
    }

    @GetMapping
    public List<Track> getTrackList() {
        return trackService.getAllTracks();
    }

    @GetMapping(value = "/{id}/download", produces = "audio/mpeg")
    public ResponseEntity<FileSystemResource> downloadTrack(@PathVariable("id") Integer trackId) throws FileNotFoundException, MalformedURLException {
        return trackService.downloadTrack(trackId);
    }

    @GetMapping(value = "/{id}")
    public Track getTrackById(@PathVariable("id") Integer trackId){
        return trackService.getTrackById(trackId);
    }
}
