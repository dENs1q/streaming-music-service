package com.musicsevice.controller;

import com.musicsevice.model.Genre;
import com.musicsevice.service.GenreService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;

@RestController
@CrossOrigin(methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.DELETE})
@RequestMapping(value = "/api/genre", produces = MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
public class GenreController {


    private final GenreService genreService;

    @PostMapping
    public Genre addAuthor(Genre genre) {
        return genreService.saveGenre(genre);
    }

    @GetMapping(value = "/{id}")
    public Genre getGenreById(@PathVariable Integer id) {
        return genreService.getGenreById(id);
    }

}
