package com.musicsevice.controller;

import com.musicsevice.model.Role;
import com.musicsevice.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
@CrossOrigin(methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.DELETE})
@RequestMapping(value = "/api/role", produces = MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
public class RoleController {


    private final RoleService roleService;


    @PostMapping
    public Role addRole(Role role) {
        return roleService.saveRole(role);
    }

    @GetMapping(value = "/{id}")
    public Role getRoleById(@PathVariable Integer id) {
        return roleService.getRoleById(id);
    }

    @GetMapping
    public List<Role> getAllRoles() {
        return roleService.getAllRole();
    }

}
