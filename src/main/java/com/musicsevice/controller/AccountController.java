package com.musicsevice.controller;

import com.musicsevice.model.Account;
import com.musicsevice.rest.AccountDTO;
import com.musicsevice.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;

@RestController
@CrossOrigin(
        methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.DELETE})
@RequestMapping(value = "/api/account", produces = MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;


    @GetMapping(value = "/{id}")
    public AccountDTO getAccountById(@PathVariable("id") Integer accountId) {
        return new AccountDTO(accountService.getById(accountId));
    }

    @PostMapping
    public AccountDTO createAccount(@RequestBody AccountDTO accountDTO) throws NoSuchAlgorithmException {
        return new AccountDTO(accountService.createAccount(new Account(accountDTO)));
    }

}
