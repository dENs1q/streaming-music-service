package com.musicsevice.controller;

import com.musicsevice.model.Account;
import com.musicsevice.rest.AccountDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin(methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.DELETE})
@RequiredArgsConstructor
@RequestMapping(value = "/api/security")
public class SecurityController {


    @GetMapping("/auth")
    @ResponseBody
    public AccountDTO user() {
        return new AccountDTO((Account) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }


}
