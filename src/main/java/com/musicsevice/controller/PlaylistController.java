package com.musicsevice.controller;

import com.musicsevice.model.Playlist;
import com.musicsevice.model.Track;
import com.musicsevice.service.PlaylistService;
import com.musicsevice.service.TrackService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
@CrossOrigin(methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.DELETE})
@RequestMapping(value = "/api/playlist", produces = MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
public class PlaylistController {


    private final PlaylistService playlistService;
    private final TrackService trackService;

    @PostMapping
    public Playlist addPlaylist(Playlist playlist) {
        return playlistService.savePlaylist(playlist);
    }

    @GetMapping(value = "/account/{accountId}")
    public List<Playlist> getAllPlaylist(@PathVariable Integer accountId) {
        return playlistService.getAllPlaylistByAccountId(accountId);
    }

    @GetMapping(value = "/{id}")
    public Playlist getPlaylistById(@PathVariable("id") Integer playlistId) {
        return playlistService.getPlaylistById(playlistId);
    }

    @GetMapping(value = "/{id}/tracks")
    public List<Track> getTracksByPlaylistId(@PathVariable("id") Integer playlistId) {
        return trackService.getByPlaylistId(playlistId);
    }
}
