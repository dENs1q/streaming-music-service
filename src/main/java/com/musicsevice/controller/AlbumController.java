package com.musicsevice.controller;

import com.musicsevice.model.Album;
import com.musicsevice.service.AlbumService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;

@RestController
@CrossOrigin(methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.DELETE})
@RequestMapping(value = "/api/album", produces = MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
public class AlbumController {


    private final AlbumService albumService;


    @PostMapping
    public Album addAlbum(Album album) {
        return albumService.saveAlbum(album);
    }

    @GetMapping(value = "/{id}")
    public Album getAlbumById(@PathVariable Integer id) {
        return albumService.getAlbumById(id);
    }

}
