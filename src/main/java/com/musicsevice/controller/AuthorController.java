package com.musicsevice.controller;

import com.musicsevice.model.Author;
import com.musicsevice.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
@CrossOrigin(methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS, RequestMethod.DELETE})
@RequestMapping(value = "/api/author", produces = MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
public class AuthorController {


    private final AuthorService authorService;



    @PostMapping
    public Author addAuthor(Author author) {
        return authorService.saveAuthor(author);
    }

    @GetMapping(value = "/{id}")
    public Author getAuthorById(@PathVariable Integer id) {
        return authorService.getAuthorById(id);
    }
    @GetMapping
    public List<Author> getAllAuthors() {
        return authorService.getAllAuthor();
    }

}
