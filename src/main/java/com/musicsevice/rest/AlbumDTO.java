package com.musicsevice.rest;

import com.musicsevice.model.Author;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlbumDTO {

    private Integer id;
    private String name;
    private String year;
    private Author author;

}
