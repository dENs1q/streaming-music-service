package com.musicsevice.rest;

import com.musicsevice.model.Album;
import com.musicsevice.model.Author;
import com.musicsevice.model.Genre;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TrackDTO {

    private Integer id;
    private String name;
    private String path;
    private String dateAdd;
    private Integer authorId;
    private Integer albumId;
    private Integer genreId;

}
