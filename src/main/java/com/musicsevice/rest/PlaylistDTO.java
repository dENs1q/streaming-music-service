package com.musicsevice.rest;

import com.musicsevice.model.Account;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlaylistDTO {

    private Integer id;
    private String name;
    private Account account;
    private Set<Integer> trackIds;

}
