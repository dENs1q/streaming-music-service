package com.musicsevice.rest;

import com.musicsevice.model.Account;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountDTO {

    private Integer id;
    private String firstname;
    private String lastname;
    private String email;
    private String password;
    private String role;
    private Set<Integer> favoriteTrackIds;

    public AccountDTO(Account account) {
        this.id = account.getId();
        this.firstname = account.getFirstname();
        this.lastname = account.getLastname();
        this.email = account.getEmail();
        this.role = account.getRole();
        this.favoriteTrackIds = account.getFavoriteTrackIds();
    }
}
