package com.musicsevice.repository;

import com.musicsevice.model.Genre;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.NoSuchElementException;

public interface GenreRepository extends JpaRepository<Genre, Integer> {

    default Genre findOneMustExists(Integer genreId) {
        return findById(genreId).orElseThrow(() -> new NoSuchElementException(String.format("Genre with id = %S not found", genreId)));
    }
}
