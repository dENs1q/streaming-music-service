package com.musicsevice.repository;

import com.musicsevice.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.NoSuchElementException;

public interface AuthorRepository extends JpaRepository<Author, Integer> {

    default Author findOneMustExists(Integer authorId) {
        return findById(authorId).orElseThrow(() -> new NoSuchElementException(String.format("Author with id = %S not found", authorId)));
    }
}
