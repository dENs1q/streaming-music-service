package com.musicsevice.repository;

import com.musicsevice.model.Track;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.ws.rs.QueryParam;
import java.util.List;
import java.util.NoSuchElementException;

public interface TrackRepository extends JpaRepository<Track, Integer> {

    default Track findOneMustExists(Integer trackId) {
        return findById(trackId).orElseThrow(() -> new NoSuchElementException(String.format("Track with id = %S not found", trackId)));
    }


    List<Track> findByAlbum_Id(Integer albumId);

    @Query(nativeQuery = true, value =
            "select t.* " +
            "from playlist p " +
            "   join playlist_to_track ptt on p.id = ptt.playlist_id " +
            "   join track t on t.id = ptt.track_id " +
            "where p.id = :playlistId")
    List<Track> findByPlayListId(Integer playlistId);

}
