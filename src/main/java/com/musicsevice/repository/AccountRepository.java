package com.musicsevice.repository;

import com.musicsevice.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.NoSuchElementException;

public interface AccountRepository extends JpaRepository<Account, Integer> {

    default Account findOneMustExists(Integer accountId) {
        return findById(accountId).orElseThrow(() -> new NoSuchElementException(String.format("Account with id = %S not found", accountId)));
    }

    Account getAccountByEmail(String accountEmail);

    boolean findAccountByEmail(String accountEmail);

}
