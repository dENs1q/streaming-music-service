package com.musicsevice.repository;

import com.musicsevice.model.Album;
import com.musicsevice.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.NoSuchElementException;

public interface AlbumRepository extends JpaRepository<Album, Integer> {

    default Album findOneMustExists(Integer albumId) {
        return findById(albumId).orElseThrow(() -> new NoSuchElementException(String.format("Album with id = %S not found", albumId)));
    }
}
