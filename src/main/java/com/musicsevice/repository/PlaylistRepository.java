package com.musicsevice.repository;

import com.musicsevice.model.Playlist;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.NoSuchElementException;

public interface PlaylistRepository extends JpaRepository<Playlist, Integer> {

    default Playlist findOneMustExists(Integer playlistId) {
        return findById(playlistId).orElseThrow(() -> new NoSuchElementException(String.format("Playlist with id = %S not found", playlistId)));
    }

    List<Playlist> findByAccountId(Integer accountId);

}
