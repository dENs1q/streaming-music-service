package com.musicsevice.repository;

import com.musicsevice.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.NoSuchElementException;

public interface RoleRepository extends JpaRepository<Role, Integer> {

    default Role findOneMustExists(Integer roleId) {
        return findById(roleId).orElseThrow(() -> new NoSuchElementException(String.format("Role with id = %S not found", roleId)));
    }

}
