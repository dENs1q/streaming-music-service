package com.musicsevice.securingweb;

import com.google.common.collect.ImmutableMap;
import com.musicsevice.service.AccountService;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.DelegatingAuthenticationEntryPoint;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.util.LinkedHashMap;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final AccountService accountService;

    public WebSecurityConfig(AccountService accountService) {
        this.accountService = accountService;
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        BasicAuthenticationEntryPoint basicAuthenticationEntryPoint = new BasicAuthenticationEntryPoint();
        basicAuthenticationEntryPoint.setRealmName("music-service");

        DelegatingAuthenticationEntryPoint entryPoint = new DelegatingAuthenticationEntryPoint(new LinkedHashMap<>(
                ImmutableMap.of(
                        //new AntPathRequestMatcher("/api/security/auth"), basicAuthenticationEntryPoint,
                        new AntPathRequestMatcher("/**"), new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED))));
        entryPoint.setDefaultEntryPoint(basicAuthenticationEntryPoint);

        http.csrf().disable().authorizeRequests().antMatchers(HttpMethod.OPTIONS, "/**").permitAll().anyRequest()
                .authenticated()
                .and().httpBasic()
                .authenticationEntryPoint(entryPoint);
        ;
    }

}
