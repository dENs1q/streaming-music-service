package com.musicsevice.service.impl;

import com.musicsevice.model.Track;
import com.musicsevice.repository.TrackRepository;
import com.musicsevice.service.TrackService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class TrackServiceImpl implements TrackService {

    private final TrackRepository trackRepository;

    @Value("${music-service.path}")
    private String path;


    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Track uploadTrack(MultipartFile file) throws Exception {

        String fileName = UUID.randomUUID().toString();
        File savedFile = new File(path, fileName);

        Track newTrack = new Track(file.getOriginalFilename(), fileName);
        newTrack = trackRepository.save(newTrack);


        if (!savedFile.exists()) {
            file.transferTo(savedFile);
        } else {
            throw new IllegalArgumentException(String.format("Track with name = '%s' already exists by path = '%s'", newTrack.getName(), path));
        }

        return newTrack;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public List<Track> getAllTracks() {
        return trackRepository.findAll();
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public ResponseEntity<FileSystemResource> downloadTrack(Integer trackId) {
        Track track = trackRepository.findOneMustExists(trackId);
        File savedTrack = new File(path, track.getFileName());

        if (!savedTrack.exists()) {
            throw new IllegalArgumentException("File not found! Path: '" + path + "'");
        }
        FileSystemResource resource = new FileSystemResource(savedTrack);
        ResponseEntity<FileSystemResource> body = ResponseEntity.ok()
                .body(resource);
        return body;

    }

    @Override
    public List<Track> getByPlaylistId(Integer playlistId) {
        return trackRepository.findByPlayListId(playlistId);
    }

    @Override
    public Track getTrackById(Integer trackId) {
        return trackRepository.findOneMustExists(trackId);
    }
}
