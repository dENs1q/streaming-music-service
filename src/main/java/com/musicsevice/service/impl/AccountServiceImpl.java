package com.musicsevice.service.impl;

import com.musicsevice.model.Account;
import com.musicsevice.repository.AccountRepository;
import com.musicsevice.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    @Override
    public Account getById(Integer id) {
        return accountRepository.findOneMustExists(id);
    }

    @Override
    public Account createAccount(Account account) throws NoSuchAlgorithmException {
        String password = account.getPassword();
        byte[] md5s = MessageDigest.getInstance("MD5").digest(password.getBytes());
        account.setPassword("{MD5}" + new BigInteger(1,md5s).toString(16));
        return accountRepository.save(account);
    }

    @Override
    public void deletedAccountById(Integer id) {
        accountRepository.deleteById(id);
    }

    @Override
    public Account getByEmail(String email) {
        return accountRepository.getAccountByEmail(email);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return getByEmail(username);
    }
}
