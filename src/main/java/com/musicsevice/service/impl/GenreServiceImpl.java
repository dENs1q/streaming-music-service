package com.musicsevice.service.impl;

import com.musicsevice.model.Genre;
import com.musicsevice.repository.GenreRepository;
import com.musicsevice.service.GenreService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GenreServiceImpl implements GenreService {

    private final GenreRepository genreRepository;


    @Override
    public Genre getGenreById(Integer id) {
        return genreRepository.findOneMustExists(id);
    }

    @Override
    public Genre saveGenre(Genre genre) {
        return genreRepository.save(genre);
    }
}
