package com.musicsevice.service.impl;

import com.musicsevice.model.Album;
import com.musicsevice.repository.AlbumRepository;
import com.musicsevice.service.AlbumService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AlbumServiceImpl implements AlbumService {

    private final AlbumRepository albumRepository;


    @Override
    public Album getAlbumById(Integer id) {
        return albumRepository.findOneMustExists(id);
    }

    @Override
    public Album saveAlbum(Album album) {
        return albumRepository.save(album);
    }
}
