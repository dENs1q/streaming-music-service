package com.musicsevice.service.impl;

import com.musicsevice.model.Playlist;
import com.musicsevice.repository.PlaylistRepository;
import com.musicsevice.service.PlaylistService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PlaylistServiceImpl implements PlaylistService {

    private final PlaylistRepository playlistRepository;

    @Override
    public Playlist getPlaylistById(Integer playlistId) {
        return playlistRepository.findOneMustExists(playlistId);
    }

    @Override
    public List<Playlist> getAllPlaylistByAccountId(Integer accountId) {
        return playlistRepository.findByAccountId(accountId);
    }

    @Override
    public Playlist savePlaylist(Playlist playlist) {
        return playlistRepository.save(playlist);
    }

}
