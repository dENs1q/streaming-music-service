package com.musicsevice.service;

import com.musicsevice.model.Account;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.security.NoSuchAlgorithmException;

public interface AccountService extends UserDetailsService {

    Account getById(Integer id);

    Account createAccount(Account account) throws NoSuchAlgorithmException;

    void deletedAccountById(Integer id);

    Account getByEmail(String email);
}
