package com.musicsevice.service;

import com.musicsevice.model.Role;

import java.util.List;

public interface RoleService {

    Role getRoleById(Integer id);

    List<Role> getAllRole();

    Role saveRole(Role role);

}
