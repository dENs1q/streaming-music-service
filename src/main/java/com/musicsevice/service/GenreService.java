package com.musicsevice.service;

import com.musicsevice.model.Genre;

public interface GenreService {

    Genre getGenreById(Integer id);

    Genre saveGenre(Genre genre);

}
