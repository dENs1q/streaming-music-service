package com.musicsevice.service;

import com.musicsevice.model.Track;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.List;

public interface TrackService {
    Track uploadTrack(MultipartFile file) throws Exception;

    List<Track> getAllTracks();

    Track getTrackById(Integer trackId);

    ResponseEntity<FileSystemResource> downloadTrack(Integer trackId) throws FileNotFoundException, MalformedURLException;

    List<Track> getByPlaylistId(Integer playlistId);
}
