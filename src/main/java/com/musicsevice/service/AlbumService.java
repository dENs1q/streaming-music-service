package com.musicsevice.service;

import com.musicsevice.model.Album;

public interface AlbumService {

    Album getAlbumById(Integer id);

    Album saveAlbum(Album album);

}
