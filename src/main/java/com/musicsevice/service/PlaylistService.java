package com.musicsevice.service;

import com.musicsevice.model.Playlist;

import java.util.List;

public interface PlaylistService {

    Playlist getPlaylistById(Integer playlistId);

    List<Playlist> getAllPlaylistByAccountId(Integer accountId);

    Playlist savePlaylist(Playlist playlist);

}
