package com.musicsevice.service;

import com.musicsevice.model.Author;

import java.util.List;

public interface AuthorService {

    Author getAuthorById(Integer id);

    List<Author> getAllAuthor();

    Author saveAuthor(Author author);

}
