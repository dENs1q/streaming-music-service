ALTER TABLE account DROP constraint account_role_id_fkey;
DROP table role;
ALTER TABLE account drop column role_id;
ALTER TABLE account add column role text;
UPDATE account SET role = 'ROLE_USER';
